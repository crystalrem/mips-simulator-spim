pair<bool, int> get_register(char *s, int p)
{
	pair<bool, int> ret;
	if(s[p] == '$')
	{
		ret.first = true;
		if(is_number(s[p + 1])) ret.second = get_num(s, p + 1);
		else
		{
			string tmp = "";
			for (int i = p + 1; s[i] && is_visible(s[i]) && s[i] != ','; i++)
				tmp += s[i];
			for (int i = 0; i <= 34; i++)
				if(tmp == register_name[i]) ret.second = i;
			if(ret.second == 32) ret.second = 30;
		}
	}
	else 
	{
		ret.first = false;
		ret.second = get_num(s, p);
	}
	return ret;
}
bool int_overflow(long long tmp)
{
	return tmp >= (-2147483648ll) && tmp <= (2147483647ll); 
}
bool unsigned_int_overflow(unsigned long long tmp)\
{
	return tmp <= (1ll << 32);
}
unsigned int rol(unsigned int now, int p)
{
	if(p < 0) ror(now, -p);
	else
	{
		unsigned int re = ((now << p) | (now >> (32 - p)));
	}
}
unsigned int ror(unsigned int now, int p)
{
	if(p < 0) rol(now, -p);
	else
	{
		unsigned int re = ((now >> p) | (now << (32 - p)));
	}
}
void work()
{
    int l = 0, r = 1;
    cmd[1] = text_notes["main"];
    while(l < r)
    {
        now = cmd[++l];
        cmd[++r] = now + 1;
		string s = "";
		for (p = 1; p <= text.length && is_visible(text[now].s[p]); p++)
			s += text[now].s[p];
		if(s == "") continue;
		while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
		switch(s)
		{
			pair<bool, int> dest = get_register(text[now].s, p);
			while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
			p++;
			while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
			pair<bool, int> src1 = get_register(text[now].s, p);
			while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
			pair<bool, int> src2 = make_pair(true, -1);
			if(text[now].s[p] == ',')
			{
				p++;
				while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
				pair<bool, int> src2 = get_register(text[now].s, p);
			}	
			case "abs":
			{
				regist[dest.second]= (unsigned int)(abs((int)regist[src1.second]));
				break;
			}
			case "add":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 + t2);
				break;
			}
			case "addi":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 + t2);
				break;
			}
			case "addu":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = t1 + t2;
				break;
			}
			case "addiu":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = t1 + t2;
				break;
			}
			case "and":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 & t2);
				break;
			}
			case "andi":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 & t2);
				break;
			}
			case "div":
			{
				if(src2.second == -1 && src2.first)
				{
					src2 = src1, src1 = dest;
					dest = make_pair(true, -1);
				}
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				if(dest.second != -1) regist[dest.second] = (int)(t1 / t2);
				regist[33] = (int)(t1 / t2);
				regist[34] = (int)(t1 % t2);
				break;
			}
			case "divu":
			{
				if(src2.second == -1 && src2.first)
				{
					src2 = src1, src1 = dest;
					dest = make_pair(true, -1);
				}
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				if(dest.second != -1) regist[dest.second] = (t1 / t2);
				regist[33] = (t1 / t2);
				regist[34] = (t1 % t2);
				break;				
			}
			case "mul":
			{
				if(src2.second == -1 && src2.first)
				{
					src2 = src1, src1 = dest;
					dest = make_pair(true, -1);
				}
				long long t1 = (long long)((int)regist[src1.second]);
				long long t2 = (long long)((int)(src.first ? regist[src2.second] : src2.second));
				long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				regist[dest.second] = regist[33];
				break;
			}
			case "mult":
			{
				src2 = src1, src1 = dest;
				dest = make_pair(true, -1);
				long long t1 = (long long)((int)regist[src1.second]);
				long long t2 = (long long)((int)(src.first ? regist[src2.second] : src2.second));
				long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				break;
			} 
			case "multu":
			{
				src2 = src1, src1 = dest;
				dest = make_pair(true, -1);
				unsigned long long t1 = (unsigned long long)(regist[src1.second]);
				unsigned long long t2 = (unsigned long long)(src.first ? regist[src2.second] : src2.second);
				unsigned long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				regist[dest.second] = (unsigned int)tmp;
				break;
			}
			case "mulo":
			{
				long long t1 = (long long)((int)regist[src1.second]);
				long long t2 = (long long)((int)(src.first ? regist[src2.second] : src2.second));
				long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				regist[dest.second] = (int)tmp;
				//if(int_overflow(tmp)) exceptions();
				break;
			}
			case "mulou":
			{
				unsigned long long t1 = (unsigned long long)(regist[src1.second]);
				unsigned long long t2 = (unsigned long long)(src.first ? regist[src2.second] : src2.second);
				unsigned long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				regist[dest.second] = (unsigned int)tmp;
				//if(unsigned_int_overflow(tmp)) exceptions();
				break;
			}
			case "neg":
			{
				int t1 = (int)(regist[src1.second]);
				regist[dest.second] = (unsigned int)(-t1);
				break;
			}
			case "negu":
			{
				int t1 = (int)(regist[src1.second]);
				regist[dest.second] = (unsigned int)(-t1);
				break;
			}
			case "nor":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = ((~t1) & (~t2));
				break;
			}
			case "not":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				regist[dest.second] = (~t1);
			}
			case "or":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 | t2);
				break;
			}
			case "ori":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 | t2);
				break;
			}
			case "rem":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 % t2);
				break;
			}
			case "remu":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 % t2);
				break;
			}
			case "rol":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = rol(t1, t2);
				break;
			}
			case "ror":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = ror(t1, t2);
				break;
			}
			case "sll":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 << t2);
				break;
			}
			case "sllv":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 << (t2 % 32));
				break;
			}
			case "sra":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 >> t2);
				break;
			}
			case "srav":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 >> (t2 % 32));
				break;
			}
			case "srl":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 >> t2);
				break;
			}
			case "srlv":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 >> (t2 % 32));
				break;
			}
			case "sub"://
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 - t2);
				if(!int_overflow((long long) (t1 - t2))) exceptions();
			}
			case "subu":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = t1 - t2;
				break;
			}
			case "xor":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 ^ t2);
				break;
			}
			case "xori":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = ((~t1) & (~t2));
				break;
			}
			case "slt":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 < t2);
				break;
			}
			case "sl"
			
		}
        
    } 
}
