CC = g++
CFLAGS = -std=c++11 -O2

_OBJ = main.o 
	
_DEPS = definition.h\
        auxiliary.h \
		dataprepare.h \
		operatorFunction.h
		
		

ODIR = bin
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

IDIR = .
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

$(ODIR)/%.o : %.cpp  $(DEPS)
	 @mkdir $(ODIR) -p
	 $(CC) -c -o $@ $< $(CFLAGS)

mips-simulator : $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)
	cp ./mips-simulator ./bin/mips-simulator

.PHONY : clean rebuild

all : 
	mips-simulator

clean : 
	rm -rf $(ODIR)

rebuild : 
	clean mips-simulator
