#ifndef OPERATORFUNCTION
#define OPERATORFUNCTION

#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include "definition.h"

void exceptions()
{
}

void lockStorage()
{
    doStorageLock = true;
    storageLock = true;
}

int getInt(Register now)
{
    if(now.isEmpty) return 0;
	if(now.isImm) return now.id;
	else return registerValue[now.id];
}

void abs(int now, int step)
{       
	if(step == 4) return;
	else registerValue[text[now].dest.id] = abs(getInt(text[now].src1));
}

void add(int now, int step)
{
	if(step == 4) return;
	long long tmp = (long long)(getInt(text[now].src1) + getInt(text[now].src2));
	int tmp2 = (int)tmp;
	if(tmp != tmp2) exceptions();
	registerValue[text[now].dest.id] = tmp2; 
}

void addi(int now, int step)
{
	add(now, step);
}

void addu(int now, int step)
{
	if(step == 4) return;
	unsigned int tmp = (unsigned int)getInt(text[now].src1) + (unsigned int)getInt(text[now].src2);
	registerValue[text[now].dest.id] = tmp; 
}

void addiu(int now, int step)
{
	addu(now, step);
}

void And(int now, int step)//And x and
{
	if(step == 4) return;
	int tmp = ((unsigned int)getInt(text[now].src1) & (unsigned int)getInt(text[now].src2));
	registerValue[text[now].dest.id] = tmp; 
}

void andi(int now, int step)
{
	And(now, step);
}

void Ddiv(int now, int step, bool exp)
{
	if(step == 4) return;
	int t1 = (getInt(text[now].src1));
	int t2 = (getInt(text[now].src2));
	registerValue[33] = (t1 / t2);
	registerValue[34] = (t1 % t2);
	if(!text[now].dest.isEmpty)
		registerValue[text[now].dest.id] = t1 / t2; 
}

void Div(int now, int step)
{
	Ddiv(now, step, 1);
}

void divu(int now, int step)
{
	//Ddiv(now, step, 0);
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	unsigned int t2 = (unsigned int)(getInt(text[now].src2));
	registerValue[33] = (t1 / t2);
	registerValue[34] = (t1 % t2);
	if(!text[now].dest.isEmpty)
		registerValue[text[now].dest.id] = t1 / t2; 
}

void Mul(int now, int step, int exp)
{
	if(step == 4) return;
	int t1 = (getInt(text[now].src1));
	int t2 = (getInt(text[now].src2));
	long long tmp = (long long)t1 * t2;
	registerValue[34] = (int)(((unsigned long long)tmp) >> 32);
	registerValue[33] = (int)(tmp - ((((unsigned long long)tmp) >> 32) << 32));
	int tmp2 = (int)tmp;
	if(exp && ((long long) tmp2) == tmp) exceptions();
	if(!text[now].dest.isEmpty)
		registerValue[text[now].dest.id] = tmp2;
	//printf("\n\n\nmul = %d %d %d\n\n\n", t1, t2, tmp2);
}

void Mulu(int now, int step, int exp)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	unsigned int t2 = (unsigned int)(getInt(text[now].src2));
	unsigned long long tmp = (unsigned long long)t1 * t2;
	registerValue[34] = (unsigned int)(tmp >> 32);
	registerValue[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
	unsigned int tmp2 = (unsigned int)tmp;
	if(exp && ((unsigned long long) tmp2) == tmp) exceptions();
	if(!text[now].dest.isEmpty)
		registerValue[text[now].dest.id] = tmp2;
}

void mul(int now, int step)
{
	Mul(now, step, 0);
}

void mulo(int now, int step)
{
	Mul(now, step, 1);
}

void mulou(int now, int step)
{
	Mulu(now, step, 1);
}

void mult(int now, int step)
{
	Mul(now, step, 0);
}

void multu(int now, int step)
{
	Mulu(now, step, 0);
}

void Neg(int now, int step, int exp)
{
	if(step == 4) return;
	int t1 = (int)(getInt(text[now].src1));
	registerValue[text[now].dest.id] = (-1) * t1;
	//if(exp && )exceptions();
}

void neg(int now, int step)
{
	Neg(now, step, 1);
}

void negu(int now, int step)
{
	Neg(now, step, 0);
}

void nor(int now, int step)
{
	if(step == 4) return;
	int tmp = ((~getInt(text[now].src1)) & (~getInt(text[now].src2)));
	registerValue[text[now].dest.id] = tmp; 
}

void Not(int now, int step)//Not x not
{
	if(step == 4) return;
	int tmp = (~getInt(text[now].src1));
	registerValue[text[now].dest.id] = tmp; 
}

void Or_(int now, int step)
{
	if(step == 4) return;
	int tmp = (getInt(text[now].src1) | getInt(text[now].src2));
	registerValue[text[now].dest.id] = tmp; 
}

void Or(int now, int step) //Or x or
{
	Or_(now, step);
}

void ori(int now, int step)
{
	Or_(now, step);
}

void rem(int now, int step)
{
	if(step == 4) return;
	int t1 = (getInt(text[now].src1));
	int t2 = (getInt(text[now].src2));
	//if(exp && ) exceptions();
	if(!text[now].dest.isEmpty)
		registerValue[text[now].dest.id] = t1 % t2; 
}

void remu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	unsigned int t2 = (unsigned int)(getInt(text[now].src2));
	//if(exp && ) exceptions();
	if(!text[now].dest.isEmpty)
		registerValue[text[now].dest.id] = (int)((unsigned int)(t1 % t2)); 
}
unsigned int Ror(unsigned int now, int p);

unsigned int Rol(unsigned int now, int p)
{
	if(p < 0) return Ror(now, -p);
	return ((now << p) | (now >> (32 - p)));
}
unsigned int Ror(unsigned int now, int p)
{
	if(p < 0) return Rol(now, -p);
	return ((now >> p) | (now << (32 - p)));
}
void rol(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	int t2 = (getInt(text[now].src2));
	registerValue[text[now].dest.id] = (int)Rol(t1, t2);
}

void ror(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	int t2 = (getInt(text[now].src2));
	registerValue[text[now].dest.id] = (int)Ror(t1, t2);
}

void sll(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 << t2);
}

void sllv(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 << (t2 % 32));	
}

void sra(int now, int step)
{
	if(step == 4) return;
	int t1 = (int)(getInt(text[now].src1));
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 >> t2);	
}

void srav(int now, int step)
{
	if(step == 4) return;
	int t1 = (int)(getInt(text[now].src1));
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 >> (t2 % 32));	
}

void srl(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 >> t2);	
}

void srlv(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)(getInt(text[now].src1));
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 >> (t2 % 32));	
}

void Sub(int now, int step, bool exp)
{
	if(step == 4) return;
	long long t1 = (long long)(getInt(text[now].src1));
	long long t2 = (long long)(getInt(text[now].src2));
	long long tmp = t1 - t2;
	int tmp2 = (int) tmp;
	if(exp && tmp != tmp2) exceptions();
	registerValue[text[now].dest.id] = tmp2;
}

void sub(int now, int step)
{
	Sub(now, step, 1);
}

void subu(int now, int step)
{
	Sub(now, step, 0);
}

void Xxor(int now, int step)
{
	if(step == 4) return;
	int tmp = (getInt(text[now].src1) ^ getInt(text[now].src2));
	registerValue[text[now].dest.id] = tmp;
}

void Xor(int now, int step)//Xor & xor
{
	Xxor(now, step);
}

void xori(int now, int step)
{
	Xxor(now, step);
}

void li(int now, int step)
{
	if(step == 4) return;
	int tmp = (getInt(text[now].src1));
	registerValue[text[now].dest.id] = tmp;
}

void lui(int now, int step)
{
	if(step == 4) return;
	int tmp = (getInt(text[now].src1));
	tmp = (tmp & ((1 << 16) - 1));
	registerValue[text[now].dest.id] = (tmp << 16);//
}

void seq(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 == t2);
}

void sge(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 >= t2);
}

void sgeu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)getInt(text[now].src1);
	unsigned int t2 = (unsigned int)getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 >= t2);
}

void sgt(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 > t2);	
}

void sgtu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)getInt(text[now].src1);
	unsigned int t2 = (unsigned int)getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 > t2);
}

void sle(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 <= t2);
}

void sleu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)getInt(text[now].src1);
	unsigned int t2 = (unsigned int)getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 <= t2);
}

void Slt(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 < t2);
}

void Sltu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)getInt(text[now].src1);
	unsigned int t2 = (unsigned int)getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 < t2);
}

void slt(int now, int step)
{
	Slt(now, step);
}

void slti(int now, int step)
{
	Slt(now, step);
}

void sltu(int now, int step)
{
	Sltu(now, step);
}

void sltiu(int now, int step)
{
	Sltu(now, step);
}

void sne(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	registerValue[text[now].dest.id] = (t1 != t2);	
}

void b(int now, int step)
{
	if(step == 4) return;
	stack = text[now].lineNum;
}

void beq(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	//printf("***beq %d %d\n", t1, t2);
	if(t1 == t2) stack = text[now].lineNum;
}

void beqz(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	if(t1 == 0) stack = text[now].lineNum;
}

void bge(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	if(t1 >= t2) stack = text[now].lineNum;	
}

void bgeu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)getInt(text[now].src1);
	unsigned int t2 = (unsigned int)getInt(text[now].src2);
	if(t1 >= t2) stack = text[now].lineNum;	
}

void bgez(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	if(t1 >= 0) stack = text[now].lineNum;
}

void bgezal(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	if(t1 >= 0)
	{
	    registerValue[32] = stack;
		stack = text[now].lineNum;
	}
}

void bgt(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	if(t1 > t2) stack = text[now].lineNum;
}

void bgtu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)getInt(text[now].src1);
	unsigned int t2 = (unsigned int)getInt(text[now].src2);
	if(t1 > t2) stack = text[now].lineNum;
}

void bgtz(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	if(t1 > 0) stack = text[now].lineNum;
}

void ble(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	if(t1 <= t2) stack = text[now].lineNum;	
}

void bleu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)getInt(text[now].src1);
	unsigned int t2 = (unsigned int)getInt(text[now].src2);
	if(t1 <= t2) stack = text[now].lineNum;	
}

void blez(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	if(t1 <= 0) stack = text[now].lineNum;
}

void bltzal(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	if(t1 < 0)
	{
	    registerValue[32] = stack;
		stack = text[now].lineNum;
	}	
}

void blt(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	if(t1 < t2) stack = text[now].lineNum;
}

void bltu(int now, int step)
{
	if(step == 4) return;
	unsigned int t1 = (unsigned int)getInt(text[now].src1);
	unsigned int t2 = (unsigned int)getInt(text[now].src2);
	if(t1 < t2) stack = text[now].lineNum;
}

void bltz(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	if(t1 < 0) stack = text[now].lineNum;
}

void bne(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	int t2 = getInt(text[now].src2);
	if(t1 != t2) stack = text[now].lineNum;
}

void bnez(int now, int step)
{
	if(step == 4) return;
	int t1 = getInt(text[now].src1);
	if(t1 != 0) stack = text[now].lineNum;
}

void ja(int now, int step)//j x ja
{
	if(step == 4) return;
	stack = text[now].lineNum;
}

void jal(int now, int step)
{
	if(step == 4) return;
	registerValue[32] = stack;
	stack = text[now].lineNum;
}

void jalr(int now, int step)
{
    if(step == 4) return;
	int t1 = getInt(text[now].src1);
	registerValue[32] = stack;
	stack = t1;
	
}

void jr(int now, int step)
{
	int t1 = getInt(text[now].src1);
	stack = t1;
}

int getIndex(Address address)
{
	return registerValue[address.origin.id] + address.offset;
}

void la(int now, int step)
{
	if(step == 4) 
	{
		//lockStorage();
		return;
	}
	else
	{
	   // printf("\n\nla = %d %d\n\n\n",text[now].dest.id, getIndex(text[now].address));
		text[now].addressNum = getIndex(text[now].address);
		registerValue[text[now].dest.id] = text[now].addressNum;
	}
}

int getByte(int now)
{
	int tmp = storage[now];
	return ((tmp >> 7) ? -1 : 1) * (tmp & ((1 << 7) - 1));
}

unsigned int getUnsignedByte(int now)
{
	return storage[now];
}

unsigned long long getDoubleWord(int now)
{
	unsigned long long tmp = 0llu;
	for (int i = now; i <= now + 7; i++)
		tmp = (tmp << 8) + storage[i];
	return tmp;
}

int getHalfWord(int now) //? x get half word u
{
	return (storage[now] << 8) + storage[now + 1];
}
int getWord(int now)
{
	return (storage[now] << 24) + (storage[now + 1] << 16)
		+ (storage[now + 2] << 8) + (storage[now + 3]);
}
void lb(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		registerValue[text[now].dest.id] = getByte(tmp);
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void lbu(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		registerValue[text[now].dest.id] = getByte(tmp);
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void ld(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		unsigned long long tmp2 = getDoubleWord(tmp);
		registerValue[text[now].dest.id] = (int)((unsigned long long)(tmp2 >> 32));
		registerValue[text[now].dest.id + 1] = (int)((unsigned long long)(tmp2 - (tmp2 >> 32) << 32));
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void lh(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		int tmp2 = getHalfWord(tmp);
		registerValue[text[now].dest.id] = (int)tmp2;
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void lhu(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		int tmp2 = getHalfWord(tmp);
		registerValue[text[now].dest.id] = (unsigned int)tmp2;
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void lw(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		int tmp2 = getWord(tmp);
		//printf("\n\n========================\n");
		//printf("tmp = %d tmp2 = %d\n\n", tmp, tmp2);
		//printf("========================\n\n\n");
		registerValue[text[now].dest.id] = (unsigned int)tmp2;
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void sb(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		unsigned int tmp2 = registerValue[text[now].src1.id];
		storage[tmp] = (unsigned char)tmp2;
		//start = max(tmp, start);
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void sd(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		unsigned int tmp1 = registerValue[text[now].src1.id];
		unsigned int tmp2 = registerValue[text[now].src1.id + 1];
		unsigned long long tmp0 = (unsigned long long)((unsigned long long)tmp1 << 32) + tmp2;
		for (int i = 7; i >= 0; i--)
		{
			storage[tmp + i] = (unsigned char)(tmp0 - ((tmp0 >> 8) << 8));
			tmp0 >>= 8;
		}
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void sh(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		unsigned int tmp2 = registerValue[text[now].src1.id];
		storage[tmp] = (unsigned char)(tmp2 >> 8);
		storage[tmp + 1] = (unsigned char)(tmp2 - ((tmp2 >> 8) << 8));
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}

void sw(int now, int step)
{
	if(step == 4)
	{
		lockStorage();
		int tmp = text[now].addressNum;
		unsigned int tmp2 = getInt(text[now].src1);
		for (int i = 3; i >= 0; i--)
		{
			storage[tmp + i] = (unsigned char)(tmp2 - ((tmp2 >> 8) << 8));
			tmp2 >>= 8;
		}
	}
	else
	{
		text[now].addressNum = getIndex(text[now].address);
	}
}
void move(int now, int step)
{
	if(step == 4) return;
	registerValue[text[now].dest.id] = getInt(text[now].src1);
}


void mfhi(int now, int step)
{
	if(step == 4) return;
	registerValue[text[now].dest.id] = registerValue[34];
}

void mflo(int now, int step)
{
	if(step == 4) return;
	registerValue[text[now].dest.id] = registerValue[33];
}

void nop(int now, int step)
{
	return;
}

void syscall(int now, int step)
{
    //printf("syscall %d %d %d\n", now, step, registerValue[3]);
	switch (registerValue[3])
	{
		case 1: 
		{
			if(step == 4) return;
			printf("%d", registerValue[5]);
			break;
		}
		case 4: 
		{
		    int p = 0;
			if(step == 4) lockStorage();
			else
			{
				char s[100005];
				int now = registerValue[5];
				while(storage[now] != (unsigned char)'\0') s[++p] = storage[now], now++, printf("%c", s[p]);	
			}
			break;
		}
		case 5:
		{
			if(step == 3) return;
			scanf("%d", &registerValue[3]);
			break;
		}
		case 8:
		{
			if(step == 4) lockStorage();
			else
			{
				char s[100005];
				int now = registerValue[5];
				scanf("%s", s + 1);
				for (int i = 1; s[i] != '\0'; i++)
					storage[now] = s[i], now++;
			}
			break;
		}
		case 9:
		{
			if(step == 4) lockStorage();
			else
			{
				int now = registerValue[5];
				registerValue[3] = start + 1;
				for (int i = 1; i <= now; i++)
					storage[++start] = 0;
			}
			break;
		}
		case 10:
		{
			if(step == 4) return;
			exit(0);
			break;
		}
		case 17:
		{
			if(step == 4) return;
			//exit(registerValue[5]);
			exit(0);
			break;
		}
	}
}
void (*operatorFunction[])(int, int) = {NULL, abs, add, addi, addu, addiu, And, andi, Div, divu, mul, mulo, mulou, mult, multu, neg, negu, nor, Not, Or, ori, rem, remu, rol, ror, sll, sllv, sra, srav, srl, srlv, sub, subu, Xor, xori, li, lui, seq, sge, sgeu, sgt, sgtu, sle, sleu, slt, slti,  sltu, sltiu, sne, b, beq, beqz, bge, bgeu, bgez, bgezal, bgt, bgtu, bgtz, ble, bleu, blez, bgezal, bltzal, blt, bltu,  bltz, bne, bnez, ja, jal, jalr, jr, la, lb, lbu, ld, lh, lhu, lw, sb, sd, sh, sw, move, mfhi, mflo, nop, syscall};
#endif

