#ifndef DEFINITION
#define DEFINITION

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
using namespace std;
char *code = NULL;
const int MaxStringSize = 1e5;
const int MaxStorageSize = 2147483647;
const int CommandSize = 88;
const int RegisterSize = 35;


class Register
{
public:
	bool isEmpty;
	bool isImm;
	int id;
	Register()
	{
		isEmpty = true;
		isImm = false;
		id = -1;
	}
	Register(bool isImm, int id) : 
		isImm(isImm), id(id)
	{
		isEmpty = false;
	}
	Register &operator = (const Register &now)
	{
	    if(this == &now) return *this;
	    this -> id = now.id;
	    this -> isEmpty = now.isEmpty;
	    this -> isImm = now.isImm;
    }
};

class Address
{
public:
	Register origin;
	int offset;
	Address() {offset = 0;}
};

class Command
{
public:
	Register dest, src1, src2;
	string label;
	int lineNum;
	Address address;
	int addressNum;
    int length, type;
    char *s;
    Command()
    {
		label = "";
        length = 0;
		type = -1;
		lineNum = -1;
        s = NULL;
    }
    Command(int len) : length(len)
    {
		label = "";
		type = -1;
		lineNum = -1;
        s = new char[len + 10];
    }   
};


char codeLine[MaxStringSize];

map<string, int> textLabels;
map<string, int> preLabels;
vector<Command> text;
vector<Command> pre;
int preLine = 0, textLine = 0;

int start = 0, end = MaxStorageSize;
map<int, unsigned char> storage;

string registerName[] = {"","zero","at","v0","v1","a0","a1","a2","a3","t0","t1","t2","t3","t4","t5","t6","t7","s0",
"s1","s2","s3","s4","s5","s6","s7","t8","t9","k0","k1","gp","sp","fp","ra","lo","hi","s8"};
int registerValue[40];
int registerLock[40];

bool storageLock = false;
bool doStorageLock = true;
bool pcLock = false;

string commandName[] = {"abs", "add", "addi", "addu", "addiu", "and", "andi", "div", "divu", "mul", "mulo", "mulou", "mult", "multu", "neg", "negu", "nor", "not", "or", "ori", "rem", "remu", "rol", "ror", "sll", "sllv", "sra", "srav", "srl", "srlv", "sub", "subu", "xor", "xori", "li", "lui", "seq", "sge", "sgeu", "sgt", "sgtu", "sle", "sleu", "slt", "slti", "sltu", "sltiu", "sne", "b", "beq", "beqz", "bge", "bgeu", "bgez", "bgezal", "bgt", "bgtu", "bgtz", "ble", "bleu", "blez", "bgezal", "bltzal", "blt", "bltu", "bltz", "bne", "bnez", "j", "jal", "jalr", "jr", "la", "lb", "lbu", "ld", "lh", "lhu", "lw", "sb", "sd", "sh", "sw", "move", "mfhi", "mflo", "nop", "syscall"};

int stack, running[10];
bool finishRunning[10];
int timesAns = 0;


#endif
