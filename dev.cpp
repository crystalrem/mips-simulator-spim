#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>
using namespace std;
FILE *mips = NULL;
const int Max_str_sz = 1e5;
const int Max_sz = 2147483647;
int pow2[40];
char mips_input[Max_str_sz];
int pre_line = 0, text_line = 0;
int start = 0, end = Max_sz;
map<int, unsigned char>storage;
map<string, int>text_notes;
map<string, int>pre_notes;
string register_name[] = {"zero", "at", "v0", "v1", "a0", "a1", "a2", "a3", "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t9", "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "k0", "k1", "gp", "sp", "fp", "ra", "s8", "lo", "hi", ""}
unsigned int regist[36];
class command
{
public:
    int length;
    char *s;
    command()
    {
        length = 0;
        s = NULL;
    }
    command(int len) : length(len)
    {
        s = new char[len + 10];
    }   
};
vector<command> text;
vector<command> pre;
int cmd[Max_str_sz];
bool is_invisibile(char ch)
{
	return (ch != 9 && ch != 32);
}
bool is_alphabet(char ch)
{
    return ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'));
}
string to_string(char *s, int p1, int p2)
{
    string ret = "";
    for (int i = p1; i <= p2; i++)
        ret += s[i];
    return ret;
}
string name_note(char *s, int now)
{
    int p2 = now - 1;
    while(p2 >= 0 && !is_visibile(s[p2])) p2--;
    int p1 = p2;
    while(p1 >= 0 && is_visibile(s[p2]) && s[p1] != ':')) p1--;
    p1++;
    return to_string(text[now].s, p1, p2);
}
string name_pre(char *s, int now)
{
    int nex = now + 1;
    while(is_alphabet(s[nex])) nex++;
    nex--;
    return to_string(s, now, nex);
}
void text_deal_note(int now)
{
    int len = text[now].length;
    int p = 1;
    for (int i = 1; i <= len; i++)
        if(text[now].s[i] == ':')
        {
            p = i + 1;
            text_notes[name_note(text[now].s, i)] = now;
        }
	while(s[p] && !is_visible(s[p])) p++;
    for (int i = p; i <= len + 1; i++)
        text.s[i - p + 1] = text.s[i];
    text[now].length = len - p + 1;
}
unsigned int get_unsigned_num(char *s, int p)
{
	unsigned int re = 0u;
	for(int i = p; is_number(s[i]); i++)
	{
		re = re * 10u + (unsigned int)(s[i] - '0');
	}
	return re;
}
int get_num(char *s, int p)
{
	int re = 0;
	bool flag = false;
	if(s[p] == '-') flag = true;
	for(int i = p; is_number(s[i]); i++)
	{
		re = re * 10 + (s[i] - '0');
	}
	if(flag) return re * (-1);
	else return re;
}
int excute_pre(string str, char *s, int p)
{
	p += 1;
	while(is_alphabet(s[p])) p++;
	p--;
	while(!is_visibile(s[p])) p++;
	p--;
	int re = start + 1;
	switch(str)
	{
		case ".align":
		{
			int tmp = get_unsigned_num(s, p);
			start+= (pow2[tmp] - start % pow2[tmp]) % pow2[tmp];
			break;
		}
		case ".ascii":
		{
			while(is_visibile(s[p]))
				storage[++start] = (unsigned char)s[p], p++;
			break;
		}
		case ".asciiz":
		{
			while(is_visibile(s[p]))
				storage[++start] = (unsigned char)s[p], p++;
			storage[++start] = (unsigned char)'\0';
			break;
		}
		case ".byte":
		{
			while(is_number(s[p]))
			{
				int tmp = get_unsigned_num(s, p);
				while(s[p] && s[p] != ',') p++;
				while(s[p] && !is_number(s[p])) p++;
				storage[++start] = (unsigned char) tmp;
			}
			break;
		}
		case ".half":
		{
			while(is_number(s[p]))
			{
				int tmp = get_unsigned_num(s, p);
				while(s[p] && s[p] != ',') p++;
				while(s[p] && !is_number(s[p])) p++;
				//tmp = tmp - ((tmp >> 16) << 16);
				storage[start += 2] = (unsigned char) (tmp >> 8);
				storage[start - 1] = (unsigned char) (tmp - ((tmp >> 8) << 8));
			}
			break;
		}
		case ".space":
		{
			int tmp = get_unsigned_num(s, p);
			for (int i = 1; i <= tmp; i++)
				storage[++start] = i;
		}
		case ".word":
		{
			int tmp = get_unsigned_num(s, p);
			while(s[p] && s[p] != ',') p++;
			while(s[p] && !is_number(s[p])) p++;
			start += 4;
			for (int i = 0; i < 4; i++)
			{
				storage[start - i] = (unsigned char) (tmp - ((tmp >> 8) << 8));
				tmp >>= 8;
			}
		}
	}
	return re;
}
void deal_pre()
{
    vector<bool>unfinish;
    vector<string>s;
    int cnt = -1;
    for (int i = 1; i <= pre_line; i++)
    {
        for (int j = 1; j <= pre[i].length; j++)
        {
            if(pre[i].s[j] == ':')
            {
                s.push_back(name_note(pre[i].s, j));
                unfinish.push_back(false);
            }
            else if(pre[i].s[j] == '.')
            {
                string now = name_pre(pre[i].s, j);
                int num = excute_pre(now, pre[i].s[j]); 
				for (int k = s.size() - 1; k >= 0 && !unfinish[k]; k--)
				{
					pre_notes[s[k]] = num;
				}
            }
        }
    }
}
void input()
{
    command *element = new command();
    text.push_back(element);
    mips_input = gets();
    bool is_text = false, is_data = false;
    while(!feof(mips))
    {
        mips_input = fgets(mips);
        if(!feof(mips))
        {
            int p = 0;
            bool tmp = false;
            while(mips_input[p]) p++;
            element = new command(p);
            for (int i = 1; i <= p; i++)
                element -> s[i] = mips_input[p - 1];
            for (int i = 1; i <= p; i++)
                if(mips_input[i - 1] == '.')
                {
                    string s = name_pre(element -> s, i);
                    if(s == ".data") 
                    {
                        tmp = true;
                        is_text = false;
                        is_data = true;
                    }
                    else if(s == ".text")
                    {
                        tmp = true;
                        is_text = true;
                        is_data = false;
                    }
                }
            if(is_text && !tmp)
            {
                text_line++;
                text_deal_note(text_line);
                text.push_back(element);
            }
            else if(is_data && !tmp)
            {
                pre_line++;
                pre.push_back(element);
            }

        }
    }
    fclose(mips); 
}
pair<bool, int> get_register(char *s, int p)
{
	pair<bool, int> ret;
	if(s[p] == '$')
	{
		ret.first = true;
		if(is_number(s[p + 1])) ret.second = get_num(s, p + 1);
		else
		{
			string tmp = "";
			for (int i = p + 1; s[i] && is_visible(s[i]) && s[i] != ','; i++)
				tmp += s[i];
			for (int i = 0; i <= 34; i++)
				if(tmp == register_name[i]) ret.second = i;
			if(ret.second == 32) ret.second = 30;
		}
	}
	else 
	{
		ret.first = false;
		ret.second = get_num(s, p);
	}
	return ret;
}
bool int_overflow(long long tmp)
{
	return tmp >= (-2147483648ll) && tmp <= (2147483647ll); 
}
bool unsigned_int_overflow(unsigned long long tmp)\
{
	return tmp <= (1ll << 32);
}
unsigned int rol(unsigned int now, int p)
{
	if(p < 0) ror(now, -p);
	else
	{
		unsigned int re = ((now << p) | (now >> (32 - p)));
	}
}
unsigned int ror(unsigned int now, int p)
{
	if(p < 0) rol(now, -p);
	else
	{
		unsigned int re = ((now >> p) | (now << (32 - p)));
	}
}
void work()
{
    int l = 0, r = 1;
    cmd[1] = text_notes["main"];
    while(l < r)
    {
        now = cmd[++l];
        cmd[++r] = now + 1;
		string s = "";
		for (p = 1; p <= text.length && is_visible(text[now].s[p]); p++)
			s += text[now].s[p];
		if(s == "") continue;
		while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
		switch(s)
		{
			pair<bool, int> dest = get_register(text[now].s, p);
			while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
			p++;
			while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
			pair<bool, int> src1 = get_register(text[now].s, p);
			while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
			pair<bool, int> src2 = make_pair(true, -1);
			if(text[now].s[p] == ',')
			{
				p++;
				while(p <= text[now].length && !is_visible(text[now].s[p])) p++;
				pair<bool, int> src2 = get_register(text[now].s, p);
			}	
			case "abs":
			{
				regist[dest.second]= (unsigned int)(abs((int)regist[src1.second]));
				break;
			}
			case "add":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 + t2);
				break;
			}
			case "addi":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 + t2);
				break;
			}
			case "addu":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = t1 + t2;
				break;
			}
			case "addiu":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = t1 + t2;
				break;
			}
			case "and":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 & t2);
				break;
			}
			case "andi":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 & t2);
				break;
			}
			case "div":
			{
				if(src2.second == -1 && src2.first)
				{
					src2 = src1, src1 = dest;
					dest = make_pair(true, -1);
				}
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				if(dest.second != -1) regist[dest.second] = (int)(t1 / t2);
				regist[33] = (int)(t1 / t2);
				regist[34] = (int)(t1 % t2);
				break;
			}
			case "divu":
			{
				if(src2.second == -1 && src2.first)
				{
					src2 = src1, src1 = dest;
					dest = make_pair(true, -1);
				}
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				if(dest.second != -1) regist[dest.second] = (t1 / t2);
				regist[33] = (t1 / t2);
				regist[34] = (t1 % t2);
				break;				
			}
			case "mul":
			{
				if(src2.second == -1 && src2.first)
				{
					src2 = src1, src1 = dest;
					dest = make_pair(true, -1);
				}
				long long t1 = (long long)((int)regist[src1.second]);
				long long t2 = (long long)((int)(src.first ? regist[src2.second] : src2.second));
				long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				regist[dest.second] = regist[33];
				break;
			}
			case "mult":
			{
				src2 = src1, src1 = dest;
				dest = make_pair(true, -1);
				long long t1 = (long long)((int)regist[src1.second]);
				long long t2 = (long long)((int)(src.first ? regist[src2.second] : src2.second));
				long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				break;
			} 
			case "multu":
			{
				src2 = src1, src1 = dest;
				dest = make_pair(true, -1);
				unsigned long long t1 = (unsigned long long)(regist[src1.second]);
				unsigned long long t2 = (unsigned long long)(src.first ? regist[src2.second] : src2.second);
				unsigned long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				regist[dest.second] = (unsigned int)tmp;
				break;
			}
			case "mulo":
			{
				long long t1 = (long long)((int)regist[src1.second]);
				long long t2 = (long long)((int)(src.first ? regist[src2.second] : src2.second));
				long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				regist[dest.second] = (int)tmp;
				//if(int_overflow(tmp)) exceptions();
				break;
			}
			case "mulou":
			{
				unsigned long long t1 = (unsigned long long)(regist[src1.second]);
				unsigned long long t2 = (unsigned long long)(src.first ? regist[src2.second] : src2.second);
				unsigned long long tmp = t1 * t2;
				regist[34] = (unsigned int)(tmp >> 32);
				regist[33] = (unsigned int)(tmp - ((tmp >> 32) << 32));
				regist[dest.second] = (unsigned int)tmp;
				//if(unsigned_int_overflow(tmp)) exceptions();
				break;
			}
			case "neg":
			{
				int t1 = (int)(regist[src1.second]);
				regist[dest.second] = (unsigned int)(-t1);
				break;
			}
			case "negu":
			{
				int t1 = (int)(regist[src1.second]);
				regist[dest.second] = (unsigned int)(-t1);
				break;
			}
			case "nor":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = ((~t1) & (~t2));
				break;
			}
			case "not":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				regist[dest.second] = (~t1);
			}
			case "or":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 | t2);
				break;
			}
			case "ori":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 | t2);
				break;
			}
			case "rem":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 % t2);
				break;
			}
			case "remu":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 % t2);
				break;
			}
			case "rol":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = rol(t1, t2);
				break;
			}
			case "ror":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = ror(t1, t2);
				break;
			}
			case "sll":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 << t2);
				break;
			}
			case "sllv":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 << (t2 % 32));
				break;
			}
			case "sra":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 >> t2);
				break;
			}
			case "srav":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 >> (t2 % 32));
				break;
			}
			case "srl":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 >> t2);
				break;
			}
			case "srlv":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 >> (t2 % 32));
				break;
			}
			case "sub"://
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (unsigned int)(t1 - t2);
				if(!int_overflow((long long) (t1 - t2))) exceptions();
			}
			case "subu":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = t1 - t2;
				break;
			}
			case "xor":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 ^ t2);
				break;
			}
			case "xori":
			{
				unsigned int t1 = (unsigned int)regist[src1.second];
				unsigned int t2 = (unsigned int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = ((~t1) & (~t2));
				break;
			}
			case "slt":
			{
				int t1 = (int)regist[src1.second];
				int t2 = (int)(src.first ? regist[src2.second] : src2.second);
				regist[dest.second] = (t1 < t2);
				break;
			}
			case "sl"
			
		}
        
    }
    
    
    
}
int main(int argc,char *argv[])
{
    mips = freopen(argv[argc - 1], "r", stdin);
	pow2[0] = 1; 
	for (int i = 1; i <= 30; i++)
		pow2[i] = pow2[i - 1] * 2;
    input();
    work();
}