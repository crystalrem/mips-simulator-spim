#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include "definition.h"
#include "auxiliary.h"
#include "dataprepare.h"
#include "operatorFunction.h"
using namespace std;

int preExcute(string str, char *s, int p)
{
	p += 1;
	while(alphabet(s[p]) || s[p] == '.') p++;
	while(!visible(s[p])) p++;
	int re = start + 1;
    if(str == ".align")
	{
		int tmp = (1 << getNum(s, p));
		start += (tmp - start % tmp) % tmp;
		return -1;
	}
	if(str == ".ascii")
	{
	    p++;
		while(s[p])
		{
		    if(s[p] == '"' && s[p - 1] != '\\')break; 
			if(s[p] == '\\' && s[p + 1] == 'n')
		    {
		        p += 2;
		        storage[++start] = (unsigned char)10;
		    }
		    else
		    {
			    storage[++start] = (unsigned char)s[p];
			    p++;
			}
		}
	}
	if(str == ".asciiz")
	{
	    p++;
		while(s[p])
		{
		    if(s[p] == '"' && s[p - 1] != '\\')break;
		    else if(s[p] == '\\' && s[p + 1] == 'n')
		    {
		        p += 2;
		        storage[++start] = (unsigned char)10;
		    }
		    else
		    {
			    storage[++start] = (unsigned char)s[p];
			    p++;
			}
		}
		storage[++start] = (unsigned char)'\0';
	}
	if(str == ".byte")
	{
		while(number(s[p]) || s[p] == '-')
		{
			unsigned int tmp = (unsigned int)getNum(s, p);
			while(s[p] && s[p] != ',') p++;
			while(s[p] && !number(s[p]) && s[p] != '-') p++;
			storage[++start] = (unsigned char) tmp;
		}
	}
	if(str == ".half")
	{
		while(number(s[p]) || s[p] == '-')
		{
			unsigned int tmp = (unsigned int)getNum(s, p);
			while(s[p] && s[p] != ',') p++;
			while(s[p] && !number(s[p]) && s[p] != '-') p++;
			storage[++start] = (unsigned char) (tmp >> 8);
			storage[++start] = (unsigned char) (tmp - ((tmp >> 8) << 8));
		}
	}
	if(str == ".space")
	{
		int tmp = getNum(s, p);
		for (int i = 1; i <= tmp; i++)
			storage[++start] = (unsigned char)0;
	}
	if(str == ".word")
	{
		while(number(s[p]) || s[p] == '-')
		{
			unsigned int tmp = (unsigned int)getNum(s, p);
			while(s[p] && s[p] != ',') p++;
			while(s[p] && !number(s[p]) && s[p] != '-') p++;
			start += 4;
			for (int i = 0; i < 4; i++)
			{
				storage[start - i] = (unsigned char) (tmp - ((tmp >> 8) << 8));
				tmp >>= 8;
			}
		}
	}
    return re;
}

void preDeal()
{
    vector<bool>finish;
    vector<string>s;
	finish.clear();
	s.clear();
    int cnt = -1; 
    for (int i = 1; i <= preLine; i++)
    {
        for (int j = 1; j <= pre[i].length; j++)
        {
            if(pre[i].s[j] == ':')
            {
                s.push_back(getNoteName(pre[i].s, j));
                finish.push_back(false);
            }
            else if(pre[i].s[j] == '.')
            {
                string now = getPreName(pre[i].s, j);
                int num = preExcute(now, pre[i].s, j); 
				if(num)
				{
					for (int k = s.size() - 1; k >= 0 && !finish[k]; k--)
					{
						preLabels[s[k]] = num;
						finish[k] = true;
					}
				}
            }
        }
    }
}

void textDealNote(int now)
{
    int len = text[now].length;
    int p = 1;
    for (int i = 1; i <= len; i++)
        if(text[now].s[i] == ':')
        {
            p = i + 1;
            textLabels[getNoteName(text[now].s, i)] = now;  
        }
	while(text[now].s[p] && !visible(text[now].s[p])) p++;
    for (int i = p; i <= len + 1; i++)
        text[now].s[i - p + 1] = text[now].s[i];
    text[now].length = len - p + 1;
}

void input()
{
    ifstream in(code); 
    Command *element = new Command();
    text.push_back(*element);
	pre.push_back(*element);
    bool isText = false, isData = false;
    while(!in.eof())
    {
        in.getline(codeLine, MaxStringSize);
        if(!in.eof() && codeLine[0])
        {
            int len = 0;
            bool tmp = false;
            while(codeLine[len]) len++;
            element = new Command(len);
            for (int i = 1; i <= len; i++)
                element -> s[i] = codeLine[i - 1];
            element -> s[++len] = 0;
            for (int i = 1; i <= len; i++)
                if(element -> s[i] == '.')
                {
                    string s = getPreName(element -> s, i);
                    if(s == ".data") 
                    {
                        tmp = true;
                        isText = false;
                        isData = true;
                    }
                    else if(s == ".text")
                    {
                        tmp = true;
                        isText = true;
                        isData = false;
                    }
                }
            if(isText && !tmp)
            {
                textLine++;
				text.push_back(*element);
                textDealNote(textLine);
				if(text[textLine].length == 0)
				{
					textLine--;
					text.pop_back();
				}
                
            }
            else if(isData && !tmp)
            {
                preLine++;
                pre.push_back(*element);
            }

        }
    }
    in.close(); 
    /*for (int i = 1; i <= textLine; i++)
    {
        printf("****** ");
        for (int j = 1; text[i].s[j]; j++)
            printf("%c", text[i].s[j]);
        printf("\n");    
    }*/

    start = textLine;
	while(start % 4 != 0) start++;
	preDeal();    
}

void prepareCommand()
{
	for (int i = 1; i <= textLine; i++)
	{
		string s = getCommandName(text[i].s, 1);
		int p = 1;
		while(text[i].s[p] && alphabet(text[i].s[p])) p++;
		while(text[i].s[p] && !visible(text[i].s[p])) p++;
		for (int j = 1; j <= CommandSize; j++)
			if(s == commandName[j])
			{
				text[i].type = j + 1;
				break;
			}
		//(*prepareFunction[text[i].type])(i, p);
		int tmp = text[i].type;
		//printf("i = %d tmp = %d p = %d\n",i,  tmp, p);
		if(tmp == 1) calcPrepareOne(i, p);
		else if(tmp >= 2 && tmp <= 7) calcPrepareThree(i, p);
		else if(tmp >= 8 && tmp <= 9) calcPrepareTwo(i, p);
		else if(tmp >= 10 && tmp <= 12) calcPrepareThree(i, p);
		else if(tmp >= 13 && tmp <= 14) calcPrepareFour(i, p);
		else if(tmp >= 15 && tmp <= 16) calcPrepareOne(i, p);
		else if(tmp == 17) calcPrepareThree(i, p);
		else if(tmp == 18) calcPrepareOne(i, p);
		else if(tmp >= 19 && tmp <= 34) calcPrepareThree(i, p);
		else if(tmp >= 35 && tmp <= 36) calcPrepareOne(i, p);
		else if(tmp >= 37 && tmp <= 48) calcPrepareThree(i, p);
		else if(tmp == 49) branchPrepareZero(i, p);
		else if(tmp == 50) branchPrepareThree(i, p);
		else if(tmp == 51) branchPrepareTwo(i, p);
		else if(tmp >= 52 && tmp <= 53) branchPrepareThree(i, p);
		else if(tmp >= 54 && tmp <= 55) branchPrepareTwo(i, p);
		else if(tmp >= 56 && tmp <= 57) branchPrepareThree(i, p);
		else if(tmp == 58) branchPrepareTwo(i, p);
		else if(tmp >= 59 && tmp <= 60) branchPrepareThree(i, p);
		else if(tmp >= 61 && tmp <= 63) branchPrepareTwo(i, p);
		else if(tmp >= 64 && tmp <= 65) branchPrepareThree(i, p);
		else if(tmp == 66) branchPrepareTwo(i, p);
		else if(tmp == 67) branchPrepareThree(i, p);
		else if(tmp == 68) branchPrepareTwo(i, p);
		else if(tmp >= 69 && tmp <= 70) branchPrepareZero(i, p);
		else if(tmp >= 71 && tmp <= 72) branchPrepareOne(i, p);
		else if(tmp >= 73 && tmp <= 79) storePrepareZero(i, p);
		else if(tmp >= 80 && tmp <= 83) storePrepareOne(i, p);
		else if(tmp == 84) movePrepareTwo(i, p);
		else if(tmp >= 85 && tmp <= 86) movePrepareOne(i, p);
	}
}

bool checkRunning()
{
	for (int i = 1; i <= 5; i++)
		if(running[i] != -1) return true;
    return false;
}

bool prepareData(int now)
{
    //printf("id = %d\n",text[now].src1.id);
	if(!text[now].src1.isEmpty && !text[now].src1.isImm &&registerLock[text[now].src1.id]) return false;
	//printf("ok1\n");
	if(!text[now].src2.isEmpty && !text[now].src2.isImm && registerLock[text[now].src2.id]) return false;
	//printf("ok\n");
	if(text[now].type == 88) //syscall
	{
		if(registerLock[2]) return false;
		switch(registerValue[2])
		{
			case 1: 
			{
			    if(registerLock[5]) return false; 
			    break;
			}
			case 4: 
			{
			    if(registerLock[5]) return false;
			    break;
            }
			case 8: 
			{
			    if(registerLock[5] || registerLock[6]) return false;
			    break;
			}
			case 9: 
			{
			    if(registerLock[5]) return false;
			    break;
			}
			case 10: 
			{
			    if(registerLock[5]) return false;
			    break;
			}
		}
	}
	else if(text[now].type == 86) // mflo
	{
		if(registerLock[33]) return false;
	}
	else if(text[now].type == 85) // mfhi
	{
		if(registerLock[34]) return false;
	}
	if(!text[now].dest.isEmpty)
		registerLock[text[now].dest.id] ++;
	if(!text[now].address.origin.isEmpty)
	    registerLock[text[now].address.origin.id] ++;
	if(text[now].type >= 8 && text[now].type <= 14) //div & mul
	{
		registerLock[33] ++;
		registerLock[34] ++;
	}
	if(text[now].type == 70) //jal
	{
		registerLock[32] ++;
	}
	if(text[now].type == 71) //jalr
	{
		registerLock[32] ++;
	}
	if(text[now].type == 76 || text[now].type == 81) // ld & sd
	{
		registerLock[text[now].dest.id + 1] ++;
	}
	return true;
}

void finishData(int now)
{
    //printf("start\n");
    //printf("now = %d type = %d\n",now, text[now].type);
	if(!text[now].dest.isEmpty)
		registerLock[text[now].dest.id] --;
	if(!text[now].address.origin.isEmpty)
	    registerLock[text[now].address.origin.id] --;
	if(text[now].type >= 8 && text[now].type <= 14) //div & mul
	{
		registerLock[33] --;
		registerLock[34] --;
	}
	if(text[now].type == 70) //jal
	{
		registerLock[32] --;
	}
	if(text[now].type == 71) //jalr
	{
		registerLock[32] --;
	}
	if(text[now].type == 76 || text[now].type == 81) // ld & sd
	{
		registerLock[text[now].dest.id + 1] --;
	}
	//if(text[now].type == ?) jumpLock = false;
	//printf("end\n");
}
void afterRunning(int now, int step)
{
    if(step == 1)
    {
        storageLock = false;
    }
    else if(step == 2)
    {
        return;
    }
    else if(step == 3)
    {
        return;
    }
    else if(step == 4)
    {
        if(doStorageLock) 
        {
            doStorageLock = false;
            storageLock = false;
        }
    }
    else if(step == 5)
    {
        finishData(now);
        if(text[now].type >= 49 && text[now].type <= 72) 
            pcLock = false;
    }
}

int work()
{
	timesAns = 0;
	stack = textLabels["main"] + 1;
	running[1] = stack - 1;
	for (int i = 2; i <= 5; i++)
		running[i] = -1;
	storageLock = false;
	memset(registerLock, 0, sizeof(registerLock));
	int ct = 0;
	//cerr << textLine << endl;
	//for (int i = 1; i <= textLine; i++)
	//    cerr<<text[i].type<< " ";
	//cerr << endl;
	while(checkRunning() )
	{
	    ct++;
	   /* cerr << "stack" << stack << " ";
	    for (int i = 1; i <= 5; i++)
	        cerr << running[i] << " " ;
	    cerr << endl;*/
	    running[6] = -1;
	    for (int i = 1; i <= 6; i++)
	        finishRunning[i] = false;
	    doStorageLock = false;
		for (int i = 5; i >= 1; i--)
		if(running[i] != -1 && running[i + 1] == -1)
		{
	//	    printf("i = %d\n", i);
			bool ok = false;
			if(i == 1)
			{
			    ok = (!storageLock && !pcLock);
			    if(ok)
			    {
			        //cerr << "ok" << endl;
			        storageLock = true;
			        if((text[running[i]].type >= 49 && text[running[i]].type <= 72) || text[running[i]].type == 88) 
			            pcLock = true;
			    }
			}
			if(i == 2)
			{
				ok = prepareData(running[i]);
			}
			if(i == 3)
			{	
			    ok = true;			
				(*operatorFunction[text[running[i]].type])(running[i], i);
			}
			if(i == 4)
			{
				ok = true;
				(*operatorFunction[text[running[i]].type])(running[i], i);
			}
			if(i == 5)
			{
				ok = true;
			}
			if(ok) 
			{
		//	    cerr << "ok3" << endl;
				running[i + 1] = running[i];
				finishRunning[i + 1] = true; 
				running[i] = -1;				
			}
			else break;
		}
		//cerr << "ok2"  << endl;
		for (int i = 6; i >= 2; i--)
		if(finishRunning[i])
		{
		//    cerr << "finishRunning " <<"i = " << i << endl;
		    afterRunning(running[i], i - 1);
		    if(i == 6)
		    {
		        if((text[running[i]].type >= 49 && text[running[i]].type <= 72) || text[running[i]].type == 88) pcLock = false;
		    }
		}
		timesAns++;
		//if(pcLock) printf("okokok\n");
		if(!pcLock && running[1] == -1 && stack > 0 && stack <= textLine)
		{
			running[1] = stack;
			stack ++;
		}
	}
}

int main(int argc,char *argv[])
{
    code = argv[argc - 1];  
    //code = freopen(argv[argc - 1], "r", stdin);
    //freopen(argv[argc - 2], "w", stdout);
	registerValue[30] = 1000000;
    input();
    //code = freopen(argv[argc - 2], "r", stdin);
    
	prepareCommand();
	/*for (int i = 1; i <= 10; i++)
	{
        for (int j = 1; text[i].s[j]; j++)
            printf("%c", text[i].s[j]);
        printf("\n");
	    printf("type = %d\n", text[i].type);
	    printf("dest: isEmpty = %d Imm = %d, id = %d\n", text[i].dest.isEmpty, text[i].dest.isImm, text[i].dest.id);
	    printf("src1: isEmpty = %d Imm = %d, id = %d\n", text[i].src1.isEmpty, text[i].src1.isImm, text[i].src1.id);
	    printf("src2: isEmpty = %d Imm = %d, id = %d\n", text[i].src2.isEmpty, text[i].src2.isImm, text[i].src2.id);
	    cout << "Label:" << text[i].label<< " " << "&" << "LineNum = " << text[i].lineNum << endl;
	    printf("Address_________\n");
	    printf("register_origin: isEmpty = %d Imm = %d, id = %d\n",text[i].address.origin.isEmpty, text[i].address.origin.isImm, text[i].address.origin.id );
	    printf("offset: %d\n", text[i].address.offset);
	    printf("-----------------\n");
	    printf("\n\n\n");
    }*/
    timesAns = work();
    printf("The Run Times Is %d\n", timesAns);
	return 0;
}
