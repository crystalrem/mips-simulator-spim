#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
using namespace std;
FILE *code = NULL;
const int MaxStringSize = 1e5;
const int MaxStorageSize = 2147483647;
const int CommandSize = 88;
const int RegisterSize = 35;

char codeLine[MaxStringSize];
map<string, int> textLabels;
map<string, int> preLabels;
vector<Command> text;
vector<Command> pre;
int preLine = 0, textLine = 0;

int start = 0, end = MaxStorageSize;
map<int, unsigned char> storage;

string registerName[] = {};
int registerValue[36];
int registerLock[36];

string commandName[] = {};
bool (*operatorFunction)(int, int) = {};

class Register
{
	bool isEmpty;
	bool isImm;
	int id;
	Register()
	{
		isEmpty = true;
		isImm = false;
		id = -1;
	}
	Register(bool isImm, int id) : 
		isImm(isImm), id(id)
	{
		isEmpty = false;
	}
};

class Address
{
	Register origin;
	int offset;
	Address() {offset = 0;}
};

class Command
{
public:
	Register dest, src1, src2;
	String label;
	int lineNum;
	Address address;
	int addressNum;
    int length, type;
    char *s;
    command()
    {
		label = "";
        length = 0;
		type = -1;
		lineNum = -1;
        s = NULL;
    }
    command(int len) : length(len)
    {
		label = "";
		type = -1;
		lineNum = -1;
        s = new char[len + 10];
    }   
};


int stack[1000005], step[10];

bool number(char ch)
{
	return (ch >= '0' && ch <= '9');
}

bool visibile(char ch)
{
	return (ch != 9 && ch != 32);
}

bool alphabet(char ch)
{
    return ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'));
}

string toString(char *s, int p1, int p2)
{
    string ret = "";
    for (int i = p1; i <= p2; i++)
        ret += s[i];
    return ret;
}

int getNum(char *s, int p)
{
	int re = 0;
	bool flag = false;
	while(!visible(s[p])) p++;
	if(s[p] == '-')
	{
		flag = true; p++;
	}
	for(int i = p; number(s[i]); i++)
	{
		re = re * 10 + (s[i] - '0');
	}
	if(flag) return re * (-1);
	else return re;
}

string getNoteName(char *s, int now)
{
	//s[now] == ':'
    int p2 = now - 1;
    while(p2 >= 1 && !visibile(s[p2])) p2--;
    int p1 = p2;
    while(p1 >= 1 && visibile(s[p1]) && s[p1] != ':')) p1--;
    p1++;
    return toString(s, p1, p2);
}

string getPreName(char *s, int now)
{
	//s[now] = '.'
    int nex = now + 1;
    while(alphabet(s[nex])) nex++;
    nex--;
    return toString(s, now, nex);
}

string getCommandName(char *s, int now)
{
	return getPreName(s, now);
}

string getLabel(char *s, int p)
{
	string ret = "";
	while(s[p] && !visible(s[p])) p++;
	while(s[p] && visible(s[p]))
	{
		ret += s[p];
		p++;
	}
	return ret;
}

pair<int, Register> getRegister(char *s, int p)
{
	pair<int, Register> ret;
	while(s[p] && !visibile(s[p])) p++;
	if(s[p] == '$')
	{
		ret.second.isImm = false;
		ret.second.isEmpty = false;
		p++;
		if(number(s[p]))
		{
			ret.second.id = getNum(s, p);
			while(number(s[p])) p++;
			ret.first = p;
		}
		else
		{
			int last = p;
			while(alphabet(s[p]) || number(s[p])) p++
			string s = toString(s, last, p - 1);
			ret.first = p;
			for (int i = 0; i < RegisterSize; i++)
				if(s == registerName[i])
				{
					ret.second.id = i;
					break;
				}	
		}	
	}
	else
	{
		ret.second.isImm = true;
		ret.second.isEmpty = false;
		ret.second.id = getNum(s, p);
		while(s[p] && number(s[p])) p++;
		ret.first = p;
	}
	return ret;
}

Address getAddress(char *s, int p)
{
	Address ret;
	while(s[p] && !visible(s[p])) p++;
	ret.offset = getNum(s, p);
	while(number(s[p])) p++;
	while(s[p] && !visible(s[p])) p++;
	p++;//s[p] == '(';
	pair<int, Register> re = getRegister(s, p);
	ret.origin = re.second;
	return ret;
}

int preExcute(string str, char *s, int p)
{
	p += 1;
	while(alphabet(s[p])) p++;
	while(!visibile(s[p])) p++;
	int re = start + 1;
	switch(str)
	{
		case ".align":
		{
			int tmp = (1 << getNum(s, p));
			start += (tmp - start % tmp) % tmp;
			return -1;
		}
		case ".ascii":
		{
			while(visibile(s[p]))
			{
				storage[++start] = (unsigned char)s[p];
				p++;
			}
			break;
		}
		case ".asciiz":
		{
			while(visibile(s[p]))
			{
				storage[++start] = (unsigned char)s[p];
				p++;
			}
			storage[++start] = (unsigned char)'\0';
			break;
		}
		case ".byte":
		{
			while(number(s[p]) || s[p] == '-')
			{
				unsigned int tmp = (unsigned int)getNum(s, p);
				while(s[p] && s[p] != ',') p++;
				while(s[p] && !number(s[p]) && s[p] != '-') p++;
				storage[++start] = (unsigned char) tmp;
			}
			break;
		}
		case ".half":
		{
			while(number(s[p]) || s[p] == '-')
			{
				unsigned int tmp = (unsigned int)getNum(s, p);
				while(s[p] && s[p] != ',') p++;
				while(s[p] && !number(s[p]) && s[p] != '-') p++;
				storage[++start] = (unsigned char) (tmp >> 8);
				storage[++start] = (unsigned char) (tmp - ((tmp >> 8) << 8));
			}
			break;
		}
		case ".space":
		{
			int tmp = getNum(s, p);
			for (int i = 1; i <= tmp; i++)
				storage[++start] = (unsigned char)0;
		}
		case ".word":
		{
			while(number(s[p]) || s[p] == '-')
			{
				unsigned int tmp = (unsigned int)getNum(s, p);
				while(s[p] && s[p] != ',') p++;
				while(s[p] && !number(s[p]) && s[p] != '-') p++;
				start += 4;
				for (int i = 0; i < 4; i++)
				{
					storage[start - i] = (unsigned char) (tmp - ((tmp >> 8) << 8));
					tmp >>= 8;
				}
			}
		}
	}
	return re;
}

void preDeal()
{
    vector<bool>finish;
    vector<string>s;
	finish.clear();
	s.clear();
    int cnt = -1;
    for (int i = 1; i <= preLine; i++)
    {
        for (int j = 1; j <= pre[i].length; j++)
        {
            if(pre[i].s[j] == ':')
            {
                s.push_back(getNoteName(pre[i].s, j));
                finish.push_back(false);
            }
            else if(pre[i].s[j] == '.')
            {
                string now = getPreName(pre[i].s, j);
                int num = preExcute(now, pre[i].s, j); 
				if(num)
				{
					for (int k = s.size() - 1; k >= 0 && !finish[k]; k--)
					{
						preLabels[s[k]] = num;
						finish[k] = true;
					}
				}
            }
        }
    }
}

void textDealNote(int now)
{
    int len = text[now].length;
    int p = 1;
    for (int i = 1; i <= len; i++)
        if(text[now].s[i] == ':')
        {
            p = i + 1;
            textLabels[getNoteName(text[now].s, i)] = now;
        }
	while(s[p] && !visible(s[p])) p++;
    for (int i = p; i <= len + 1; i++)
        text.s[i - p + 1] = text.s[i];
    text[now].length = len - p + 1;
}

void input()
{
    Command *element = new Command();
    text.push_back(element);
	pre.push_back(element);
    bool isText = false, isData = false;
    while(!feof(code))
    {
        codeLine = fgets(code);
        if(!feof(code) && codeLine[0])
        {
            int len = 0;
            bool tmp = false;
            while(codeLine[len]) len++;
            element = new Command(len);
            for (int i = 1; i <= len; i++)
                element -> s[i] = codeLine[i - 1];
            for (int i = 1; i <= len; i++)
                if(element -> s[i] == '.')
                {
                    string s = getPreName(element -> s, i);
                    if(s == ".data") 
                    {
                        tmp = true;
                        isText = false;
                        isData = true;
                    }
                    else if(s == ".text")
                    {
                        tmp = true;
                        isText = true;
                        isData = false;
                    }
                }
            if(isText && !tmp)
            {
                textLine++;
				text.push_back(*element);
                textDealNote(textLine);
				if(text[textLine].length == 0)
				{
					textLine--;
					text.pop_back();
				}
                
            }
            else if(isData && !tmp)
            {
                preLine++;
                pre.push_back(*element);
            }

        }
    }
    fclose(code); 
	start = textLine;
	while(start % 4 != 0) start++;
	preDeal();
}

int jumpComma(char *s, int p)
{
	while(s[p] && !visible(s[p])) p++;
	if(s[p] != ',') return -1;
	p++;
	while(s[p] && !visible(s[p])) p++;
	return p;
}


void calcPrepareOne(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	ret.src1 = ret.second;
	return ;
}

void calcPrepareTwo(int now, int p)
{
	pair<int, Register> ret1, ret2;
	ret1 = getRegister(text[now].s, p);	
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret2 = getRegister(text[now].s, p);
	p = ret.first;
	p = jumpComma(text[now].s, p);
	if(p == -1)
	{
		text[now].src1 = ret1;
		text[now].src2 = ret2;
	}		
	else
	{
		text[now].dest = ret1;
		text[now].src1 = ret2;
		text[now].src2 = getRegister(text[now].s, p).second;
	}
	return ;
}

void calcPrepareThree(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	ret.src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	ret.src2 = ret.second;
	return ;
}

void branchPrepareZero(int now, int p)
{
	text[now].label = getLabel(text[now].s, p);
	return;
}

void branchPrepareOne(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	return ;
}

void branchPrepareTwo(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	text[now].label = getLabel(text[now].s, p);
	return ;
}

void branchPrepareThree(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	ret.src2 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	text[now].label = getLabel(text[now].s, p);
	return ;
}

void storePrepare(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	text[now].address = getAddress(text[now].s, p);
	return;
}

void movePrepareOne(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	return ;
}

void movePrepareTwo(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	ret.src1 = ret.second;
	return ;
}
void prepareCommand()
{
	for (int i = 1; i <= textLine; i++)
	{
		string s = getCommandName(text[i].s, 1);
		int p = 1;
		while(s[p] && alphabet(s[p])) p++;
		while(s[p] && !visible(s[p])) p++;
		for (int j = 1; j <= CommandSize; j++)
			if(s == commandName[j])
			{
				text[i].type = j;
				break;
			}
		//(*prepareFunction[text[i].type])(i, p);
		int tmp = text[i].type;
		else if(tmp == 1) calcPrepareOne(i, p);
		if(tmp >= 2 && tmp <= 7) calcPrepareThree(i, p);
		else if(tmp >= 8 && tmp <= 9) calcPrepareTwo(i, p);
		else if(tmp >= 10 && tmp <= 12) calcPrepareThree(i, p);
		else if(tmp >= 15 && tmp <= 16) calcPrepareOne(i, p);
		else if(tmp == 17) calcPrepareThree(i, p);
		else if(tmp == 18) calcPrepareOne(i, p);
		else if(tmp >= 20 && tmp <= 34) calcPrepareThree(i, p);
		else if(tmp >= 35 && tmp <= 36) calcPrepareOne(i, p);
		else if(tmp >= 37 && tmp <= 48) calcPrepareThree(i, p);
		else if(tmp == 49) branchPrepareZero(i, p);
		else if(tmp == 50) branchPrepareThree(i, p);
		else if(tmp == 51) branchPrepareTwo(i, p);
		else if(tmp >= 52 && tmp <= 53) branchPrepareThree(i, p);
		else if(tmp >= 54 && tmp <= 55) branchPrepareTwo(i, p);
		else if(tmp >= 56 && tmp <= 57) branchPrepareThree(i, p);
		else if(tmp == 58) branchPrepareTwo(i, p);
		else if(tmp >= 59 && tmp <= 60) branchPrepareThree(i, p);
		else if(tmp >= 61 && tmp <= 63) branchPrepareTwo(i, p);
		else if(tmp >= 64 && tmp <= 65) branchPrepareThree(i, p);
		else if(tmp == 66) branchPrepareTwo(i, p);
		else if(tmp == 67) branchPrepareThree(i, p);
		else if(tmp == 68) branchPrepareTwo(i, p);
		else if(tmp >= 69 && tmp <= 70) branchPrepareZero(i, p);
		else if(tmp >= 71 && tmp <= 72) branchPrepareOne(i, p);
		else if(tmp >= 73 && tmp <= 83) storePrepare(i, p);
		else if(tmp == 84) movePrepareOne(i, p);
		else if(tmp >= 85 && tmp <= 86) movePrepareTwo(i, p);
	}
}

bool checkRunning()
{
	for (int i = 1; i <= 5; i++)
		if(running[i] != -1) return true;
}

bool prepareData(int now)
{
	if(!text[now].src1.isEmpty && registerLock[now.src1.id]) return false;
	if(!text[now].src2.isEmpty && registerLock[now.src2.id]) return false;
	if(text[now].type == 88) //syscall
	{
		if(registerLock[2]) return false;
		switch(registerValue[2])
		{
			case 1: if(registerLock[4]) return false;
			case 4: if(registerLock[4]) return false;
			case 8: if(registerLock[4] || registerLock[5]) return false;
			case 9: if(registerLock[4]) return false;
			case 10: if(registerLock[4]) return false;
		}
	}
	else if(text[now].type == 86) // mflo
	{
		if(registerLock[33]) return false;
	}
	else if(text[now].type == 85) // mfhi
	{
		if(registerLock[34]) return false;
	}
	if(!text[now].dest.isEmpty)
		registerLock[text[now].dest.id] ++;
	if(text[now].type >= 8 && text[now].type <= 14) //div & mul
	{
		registerLock[33] ++;
		registerLock[34] ++;
	}
	if(text[now].type == 56)
	{
		registerLock[31]++;
	}
	if(text[now].type == 71) //jalr
	{
		registerLock[31] ++;
	}
	if(text[now].type == 76 || text[now].type == 81) // ld & sd
	{
		registerLock[text[now].dest.id + 1] = true;
	}
	return true;
}

void finishData(int now)
{
	if(!text[now].dest.isEmpty)
		registerLock[text[now].dest.id] --;
	if(text[now].type >= 8 && text[now].type <= 14) //div & mul
	{
		registerLock[33] --;
		registerLock[34] --;
	}
	if(text[now].type == 56)
	{
		registerLock[31]--;
	}
	if(text[now].type == 71) //jalr
	{
		registerLock[31] --;
	}
	if(text[now].type == 76 || text[now].type == 81) // ld & sd
	{
		registerLock[text[now].dest.id + 1] --;
	}
	//if(text[now].type == ?) jumpLock = false;
}
int work()
{
	l = 0, r = 1, timesAns = 0;
	stack[1] = textLabels["main"];
	for (int i = 1; i <= 5; i++)
		running[i] = -1;
	while(l < r || checkRunning())
	{
		storageLock = false;
		memset(registerLock, 0, sizeof(registerLock));
		for (int i = 5; i >= 1; i--)
		if(running[i] != -1)
		{
			bool ok = false;
			if(i == 1) ok = (!storageLock);
			if(i == 2)
			{
				ok = prepareData(running[i]);
			}
			if(i == 3)
			{				
				ok = (*operatorFunction[text[running[i]].type])(running[i], i);
			}
			if(i == 4)
			{
				storageLock = true;
				ok = (*operatorFunction[text[running[i]].type])(running[i], i);
			}
			if(i == 5)
			{
				ok = finishData(running[i]);
			}
			if(ok) 
			{
				if(i == 5) running[i] = -1;
				else running[i + 1] = running[i]; 
			}
			else break;
		}
		timesAns++;
		if(!jumpLock() && running[1] == -1 && q[l + 1] > 0 && q[l + 1] <= textLine)
		{
			running[1] = q[--r];
			//if(text[running[1]].type == ?) jumpLock = true;
			q[++r] = running[1] + 1;
		}
	}
}

int main(int argc,char *argv[])
{
    code = freopen(argv[argc - 1], "r", stdin);
	registerValue[29] = MaxStorageSize;
    input();
	prepareCommand();
    timesAns = work();
	printf("The Run Times Is %d\n", timesAns);
	return 0;
}