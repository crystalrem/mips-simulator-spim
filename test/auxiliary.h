#ifndef AUXILIARY
#define AUXILIARY

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include "definition.h"
using namespace std;
bool number(char ch)
{
	return (ch >= '0' && ch <= '9');
}

bool visible(char ch)
{
	return (ch != 9 && ch != 32);
}

bool alphabet(char ch)
{
    return ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'));
}

string toString(char *s, int p1, int p2)
{
    string ret = "";
    for (int i = p1; i <= p2; i++)
        ret += s[i];
    return ret;
}

int getNum(char *s, int p)
{
	int re = 0;
	bool flag = false;
	while(!visible(s[p])) p++;
	if(s[p] == '-')
	{
		flag = true; p++;
	}
	for(int i = p; number(s[i]); i++)
	{
		re = re * 10 + (s[i] - '0');
	}
	if(flag) return re * (-1);
	else return re;
}

string getNoteName(char *s, int now)
{
	//s[now] == ':'
    int p2 = now - 1;
    while(p2 >= 1 && !visible(s[p2])) p2--;
    int p1 = p2;
    while(p1 >= 1 && visible(s[p1]) && s[p1] != ':') p1--;
    p1++;
    return toString(s, p1, p2);
}

string getPreName(char *s, int now)
{
	//s[now] = '.'
    int nex = now + 1;
    while(alphabet(s[nex])) nex++;
    nex--;
    return toString(s, now, nex);
}

string getCommandName(char *s, int now)
{
	return getPreName(s, now);
}

string getLabel(char *s, int p)
{
	string ret = "";
	while(s[p] && !visible(s[p])) p++;
	while(s[p] && visible(s[p]))
	{
		ret += s[p];
		p++;
	}
	return ret;
}

pair<int, Register> getRegister(char *s, int p)
{
   /* printf("text for getRegister %d\n", p);
    for (int i = 1; s[i]; i++)
        printf("%c", s[i]);
    printf("\n");*/

	pair<int, Register> ret;
	while(s[p] && !visible(s[p])) p++;
	//printf("getregister: p = %d", p);
	if(s[p] == '$')
	{   
	  //  printf("ok\n");
		ret.second.isImm = false;
		ret.second.isEmpty = false;
		p++;
		if(number(s[p]))
		{
			ret.second.id = getNum(s, p);
			while(number(s[p])) p++;
			ret.first = p;
		}
		else
		{
			int last = p;
			while(alphabet(s[p]) || number(s[p])) p++;
			string s0 = toString(s, last, p - 1);
			ret.first = p;
			for (int i = 1; i <= RegisterSize; i++)
				if(s0 == registerName[i])
				{
					ret.second.id = i;
					break;
				}	
		}
		if(ret.second.id == 35) ret.second.id = 31;	
	}
	else
	{
		ret.second.isImm = true;
		ret.second.isEmpty = false;
		ret.second.id = getNum(s, p);
		while(s[p] && number(s[p])) p++;
		ret.first = p;
	}
	return ret;
}

Address getAddress(char *s, int p)
{
	Address ret;
	while(s[p] && !visible(s[p])) p++;
	if(s[p] == '-' || number(s[p]))
	{
	    ret.offset = getNum(s, p);
	    while(number(s[p]) || s[p] == '-') p++;
	    while(s[p] && !visible(s[p])) p++;
	    p++;//s[p] == '(';
	    pair<int, Register> re = getRegister(s, p);
	    ret.origin = re.second;
	    
	}
	else
	{
	    ret.origin.isEmpty = true;
	    int last = p;
	    while(s[p] && visible(s[p])) p++;
	    p--;
	    //printf("\n\n++++++++++++++++++++++++++\n");
	    //cout <<toString(s, last, p) << endl;
	    //cout << preLabels[toString(s, last, p)] << endl;
	    //printf("++++++++++++++++++++++++++\n\n\n");
	    ret.offset = preLabels[toString(s, last, p)];
	}
	return ret;
}

int jumpComma(char *s, int p)
{
	while(s[p] && !visible(s[p])) p++;
	if(s[p] != ',') return -1;
	p++;
	while(s[p] && !visible(s[p])) p++;
	return p;
}
#endif



