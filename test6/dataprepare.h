#ifndef DATAPREPARE
#define DATAPREPARE

#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include "definition.h"
using namespace std;
void calcPrepareOne(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	return ;
}

void calcPrepareTwo(int now, int p)
{
	pair<int, Register> ret1, ret2;
	ret1 = getRegister(text[now].s, p);	
	p = ret1.first;
	p = jumpComma(text[now].s, p);
	ret2 = getRegister(text[now].s, p);
	p = ret2.first;
	p = jumpComma(text[now].s, p);
	if(p == -1)
	{
		text[now].src1 = ret1.second;
		text[now].src2 = ret2.second;
	}		
	else
	{
		text[now].dest = ret1.second;
		text[now].src1 = ret2.second;
		text[now].src2 = getRegister(text[now].s, p).second;
	}
	return ;
}

void calcPrepareThree(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	text[now].src2 = ret.second;
	return ;
}

void calcPrepareFour(int now, int p)
{
    pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	text[now].src2 = ret.second;
	return ;
}
void branchPrepareZero(int now, int p)
{
	text[now].label = getLabel(text[now].s, p);
	text[now].lineNum = textLabels[text[now].label];
	return;
}

void branchPrepareOne(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	return ;
}

void branchPrepareTwo(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	text[now].label = getLabel(text[now].s, p);
	text[now].lineNum = textLabels[text[now].label];
	return ;
}

void branchPrepareThree(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	text[now].src2 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	text[now].label = getLabel(text[now].s, p);
	text[now].lineNum = textLabels[text[now].label];
	return ;
}

void storePrepareZero(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	text[now].address = getAddress(text[now].s, p);
	return;
}

void storePrepareOne(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	text[now].address = getAddress(text[now].s, p);
	return;
}
void movePrepareOne(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	return ;
}

void movePrepareTwo(int now, int p)
{
	pair<int, Register> ret;
	ret = getRegister(text[now].s, p);
	text[now].dest = ret.second;
	p = ret.first;
	p = jumpComma(text[now].s, p);
	ret = getRegister(text[now].s, p);
	text[now].src1 = ret.second;
	return ;
}
#endif
